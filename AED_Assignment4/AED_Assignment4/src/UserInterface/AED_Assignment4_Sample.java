/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Initialization;
import java.util.Scanner;

/**
 *
 * @author karandutt
 */
public class AED_Assignment4_Sample {
    public static void main(String[] args) {
        // TODO code application logic here

        System.out.println("*********Welcome to the EDUCATION LEVEL ECOSYSTEM*************** ");
        System.out.println("*********Please enter the choice below for Reports*************** ");
        System.out.println("1.DEPARTMENT LEVEL REPORTS");
        System.out.println("2.COLLEGE LEVEL REPORTS");
        System.out.println("3.UNIVERSITY LEVEL REPORTS");
        System.out.println("4.EDUCATION ECO SYSTEM REPORTS");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter a choice. Please enter a number for example just enter 1 for department level reports");
        int choice = reader.nextInt(); // Scans the next token of the input as an int.
        

        switch (choice) {
            case 1:
                //Retrieving the values from the Initialization file
                double gpa = Initialization.InitializeStudent().getCourseload().getSemesterGPA();
                System.out.println("***************DEPARTMENT LEVEL REPORTS STARTS******************");
                System.out.println("Calculate GPA of Karan Dutt?");
                System.out.println("GPA of Karan Dutt is " + gpa);
                System.out.println("***************DEPARTMENT LEVEL REPORT ENDS******************");
                break;
            case 2:
                System.out.println("***************COLLEGE LEVEL REPORTS STARTS******************");
                System.out.println("QUESTIONS");
                System.out.println("ANSWERS");
                System.out.println("***************COLLEGE LEVEL REPORT ENDS******************");
                break;
            case 3:
                System.out.println("***************UNIVERSITY LEVEL REPORTS STARTS******************");
                System.out.println("QUESTIONS");
                System.out.println("ANSWERS");
                System.out.println("***************UNIVERSITY LEVEL REPORT ENDS******************");
                break;
            case 4:
                System.out.println("***************EDUCATION ECO SYSTEM LEVEL REPORTS STARTS******************");
                System.out.println("QUESTIONS");
                System.out.println("ANSWERS");
                System.out.println("***************EDUCATION ECO SYSTEM LEVEL REPORT ENDS******************");
                break;
            default:
                System.out.println("INPUT ENTERED IS INCORRECT ASK USER FOR THE INPUT AGAIN EXPLORE WHILE/Do-While loops");
                break;
        }
    }

}
