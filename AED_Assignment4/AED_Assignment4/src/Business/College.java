/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class College {
    
    //declaration of private instance variables
    private String collegeName;
    private ArrayList<Department> departmentdirectory;
    
    //constructor
    public College(){
        departmentdirectory = new ArrayList<Department>();
    }
    
    //getters and setters
    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public ArrayList<Department> getDepartmentdirectory() {
        return departmentdirectory;
    }

    public void setDepartmentdirectory(ArrayList<Department> departmentdirectory) {
        this.departmentdirectory = departmentdirectory;
    }
    
    //methods
    public Department addDepartment(){
        Department department = new Department();
        departmentdirectory.add(department);
        return department;
    }
    
    public void deleteDepartment(Department department){
        departmentdirectory.remove(department);
    }
    
    @Override
    public String toString(){
        return collegeName;
    }
    
}
