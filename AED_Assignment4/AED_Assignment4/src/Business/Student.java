/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Student {
    //declaration of private instance variables
    private String studentID;
    private Studentaccount studentaccount;
    private Transcript transcript;
    private Person person;
    private Courseload courseload;
    private ArrayList<Courseload> courseloadlist;
    private int unitsEarned;
    private static final int CREDITS_TO_GRADUATE = 32;
    
    public Student(){
        studentaccount = new Studentaccount();
        courseload = new Courseload();
        courseloadlist=new ArrayList<Courseload>();
    }
    
    
    //get the all semester, whichever applicable gpa and iterate studentGPA 
    public double mainGPA(){
        int n = 0;
        double studentGPA = 0;
        for(Courseload course: courseloadlist)
    {
        studentGPA += course.semesterGPA();
        n++;
    }
        return (double)(studentGPA/n);
    
    }

    public int getUnitsEarned() {
        return unitsEarned;
    }

    public void setUnitsEarned(int unitsEarned) {
        this.unitsEarned = unitsEarned;
    }
    
    
    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public Studentaccount getStudentaccount() {
        return studentaccount;
    }

    public void setStudentaccount(Studentaccount studentaccount) {
        this.studentaccount = studentaccount;
    }

    public Transcript getTranscript() {
        return transcript;
    }

    public void setTranscript(Transcript transcript) {
        this.transcript = transcript;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Courseload getCourseload() {
        return courseload;
    }

    public void setCourseload(Courseload courseload) {
        this.courseload = courseload;
    }
    
    //methods
    public void incrementUnits(int additonalUnits){
        this.unitsEarned = additonalUnits;
    }
    
    public boolean hasEnoughUnits(){
        return (unitsEarned >= CREDITS_TO_GRADUATE);
    }
    
}
