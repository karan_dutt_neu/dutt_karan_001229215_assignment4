/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Degree {
    //declaration of private instance variables
    private String degreeName;
    //private static final int CREDITS_TO_GRADUATE = 32;
    private ArrayList<Student> studentlist;
    private ArrayList<Course> courselist;
    private Course core;
    private ArrayList<Course> electivelist; 
    
    
    //constructor
    public Degree(){
        studentlist = new ArrayList<Student>();
        courselist = new ArrayList<Course>();
    }

    //getters and setters

    public ArrayList<Course> getElectivelist() {
        return electivelist;
    }

    public void setElectivelist(ArrayList<Course> electivelist) {
        this.electivelist = electivelist;
    }
    
    
    public Course getCore() {
        return core;
    }

    public void setCore(Course core) {
        this.core = core;
    }
    
    
    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public ArrayList<Student> getStudentlist() {
        return studentlist;
    }

    public void setStudentlist(ArrayList<Student> studentlist) {
        this.studentlist = studentlist;
    }

    public ArrayList<Course> getCourselist() {
        return courselist;
    }

    public void setCourselist(ArrayList<Course> courselist) {
        this.courselist = courselist;
    }
    
    //methods
    public void addCourse(Course core, ArrayList<Course> electivelist){
        this.core = core;
        //courselist.add(core);
        for(Course course : electivelist){
            this.electivelist.add(course);
        }
        
    }
    
    public void deleteCourse(Course course){
        courselist.remove(course);
    }
    
    //add or delete a student to a particular course
    public void addStudent(Student student){
        //Student student = new Student();
        studentlist.add(student);
        //return student;
    }
    
    public void deleteStudent(Student student){
        studentlist.remove(student);
    }
    
    
    
            
}
