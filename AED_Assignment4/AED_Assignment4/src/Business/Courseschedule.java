/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Courseschedule {
    //declaration of private instance variables
    private String semester;
    private ArrayList<Courseoffering> courseofferings;
    
    //constructor
    public Courseschedule(){
        courseofferings = new ArrayList<Courseoffering>();
    }

    //getters and setters
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public ArrayList<Courseoffering> getCourseofferings() {
        return courseofferings;
    }

    public void setCourseofferings(ArrayList<Courseoffering> courseofferings) {
        this.courseofferings = courseofferings;
    }
    
    //methods
    public Courseoffering addCourseoffering(){
        Courseoffering courseoffering = new Courseoffering();
        courseofferings.add(courseoffering);
        return courseoffering;
    }
    
    public void deleteCourseoffering(Courseoffering courseoffering){
        courseofferings.remove(courseoffering);
    }
    
    public Courseoffering getCourseoffering(String courseCRN){
        for(Courseoffering courseoffering : courseofferings){
            Course course = courseoffering.getCourse();
            if(course.getCourseCRN().matches(courseCRN)){
                return courseoffering;
            }
        } 
        return null;
    }      
}
