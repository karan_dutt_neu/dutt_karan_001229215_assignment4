/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Courseload {
    //declaration of private instance variables
    
    private ArrayList<Seatassignment> seatassignmentlist;
    private double averageGrade;
    private double semesterGPA;
    
    //getters and setters
public Courseload()
{
    seatassignmentlist=new ArrayList<Seatassignment>();
}
    public double getSemesterGPA() {
        double semesterGPAvg = semesterGPA();
        return semesterGPAvg;
    }

    public void setSemesterGPA(double semesterGPA) {
        this.semesterGPA = semesterGPA;
    }
    
    
    public ArrayList<Seatassignment> getSeatassignmentlist() {
        return seatassignmentlist;
    }

    public void setSeatassignmentlist(ArrayList<Seatassignment> seatassignmentlist) {
        this.seatassignmentlist = seatassignmentlist;
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(double averageGrade) {
        this.averageGrade = averageGrade;
    }
    
    //suppose you have two subjects, it will iterate twice and it will return the semester gpa for the two subjects
    // for one subject, one gpa
    public double semesterGPA(){
        int tmp = 0;
        int semesterGPA1 = 0;
        for(Seatassignment seatAssignment: seatassignmentlist)
        {
        semesterGPA1 += seatAssignment.getGrade();
        tmp++;
        }
        return (double)(semesterGPA1/tmp);
    }   
    
    public Seatassignment addSeatassignment(){
        Seatassignment seatassignment = new Seatassignment();
        seatassignmentlist.add(seatassignment);
        return seatassignment;
    }
    
    
    
    
}
