/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Coursecatalog {
    //declaration of private instance variables
    private ArrayList<Course> coursecatalog;
    
    //constructor
    public Coursecatalog(){
        coursecatalog = new ArrayList<Course>();
    }
    
    //getters and setters
    public ArrayList<Course> getCoursecatalog() {
        return coursecatalog;
    }

    public void setCoursecatalog(ArrayList<Course> coursecatalog) {
        this.coursecatalog = coursecatalog;
    }
    
    //methods
    public Course addCourse(){
        Course course = new Course();
        coursecatalog.add(course);
        return course;
    }
    
    public void deleteCourse(Course course){
        coursecatalog.remove(course);
    }
       public Courseoffering addCourseoffering(){
        Courseoffering courseoffering = new Courseoffering();
        return courseoffering;
    }
       
    public Course getCourse(String courseCRN){
        for(Course course : coursecatalog){
            if(course.getCourseCRN().matches(courseCRN)) {
                return course;
            }
        } 
        return null;
    }
    
    
}
