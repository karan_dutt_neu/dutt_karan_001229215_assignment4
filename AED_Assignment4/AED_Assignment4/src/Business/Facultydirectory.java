/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Facultydirectory {
    
    //declaration of private instance variables
    private ArrayList<Faculty> facultydirectory;
    private int facultyCount = 0;
    
    //constructor
    public Facultydirectory(){
        facultydirectory = new ArrayList<Faculty>();
    }
    
    //getter and setter methods
    public ArrayList<Faculty> getFacultydirectory() {
        return facultydirectory;
    }

    public void setFacultydirectory(ArrayList<Faculty> facultydirectory) {
        this.facultydirectory = facultydirectory;
    }

    public int getFacultyCount() {
        return facultyCount;
    }

    public void setFacultyCount(int facultyCount) {
        this.facultyCount = facultyCount;
    }
    
    //methods
    //this method adds a faculty member to the facultydirectory and adds 1 to the facultycount.
    public Faculty addFaculty(){
        Faculty faculty = new Faculty();
        facultydirectory.add(faculty);
        facultyCount++;
        return faculty;
    }
    
    public void deleteFaculty(Faculty faculty){
        facultydirectory.remove(faculty);
        facultyCount--;
    }
    
    //this method returns a faculty member from the facultydirectory
    public Faculty getFaculty(String facultyID){
        for(Faculty faculty : facultydirectory){
            if(faculty.getFacultyID().matches(facultyID)){
                return faculty;
            }
        }
        return null;
    }
    

}
