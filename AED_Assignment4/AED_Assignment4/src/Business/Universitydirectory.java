/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Universitydirectory {
    //declaration of private instance variables
    private ArrayList<University> universitydirectory;
    
    //constructor
    public Universitydirectory(){
        universitydirectory = new ArrayList<University>();
    }
    
    //getters and setters
    public ArrayList<University> getUniversitydirectory() {
        return universitydirectory;
    }

    public void setUniversitydirectory(ArrayList<University> universitydirectory) {
        this.universitydirectory = universitydirectory;
    }
    
    
    
    
}
