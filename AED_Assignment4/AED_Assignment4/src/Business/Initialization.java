/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;



/**
 *
 * @author karandutt
 */

public class Initialization {
    
    
    public static Student InitializeStudent(){
        
        //region 1 
        Region boston = new Region();
         
        //university1 in boston region
        University university1 = boston.addUniversity();
        university1.setUniversityName("University of Massachusetts Boston");
        System.out.println(university1);
        
        //university2 in boston region
        University university2 = boston.addUniversity();
        university2.setUniversityName("Boston University");
        System.out.println(university2);
         
        
        //region2
        Region sanfrancisco = new Region();
        
        //university3 in region2
        University university3 = sanfrancisco.addUniversity();
        university3.setUniversityName("Santa Clara University");
        System.out.println(university3);
        
        //university4 in region2
        University university4 = sanfrancisco.addUniversity();
        university4.setUniversityName("San Jose State University");
        System.out.println(university4);
        
        
        //add college1 in the university1
        College college1 = university1.addCollege();
        college1.setCollegeName("College of Management UMASS Boston");
        System.out.println(college1);
        
        //add college2 in university1
        College college2 = university1.addCollege();
        college2.setCollegeName("College of Law UMASS Boston");
        System.out.println(college2);
        
        //add college3 in university2
        College college3 = university2.addCollege();
        college3.setCollegeName("College of Management Boston University");
        System.out.println(college3);
        
        //add college4 in university2
        College college4 = university2.addCollege();
        college4.setCollegeName("College of Law Boston University");
        System.out.println(college4);
        
        //add department1 in university1/college1
        Department department1 = college1.addDepartment();
        department1.setDepartmentName("Dept1 College of Management UMASS BOSTON");
        System.out.println(department1);
        
        //add department2 in university1/college1
        Department department2 = college1.addDepartment();
        department2.setDepartmentName("Dept2 College of Management UMASS BOSTON");
        System.out.println(department2);
        
        //add department3 in university1/college2
        Department department3 = college2.addDepartment();
        department3.setDepartmentName("Dept1 College of Law UMASS Boston");
        System.out.println(department3);
        
        //add department4 in university1/college2
        Department department4 = college2.addDepartment();
        department4.setDepartmentName("Dept2 College of Law UMASS Boston");
        System.out.println(department4);
        
        //add department5 in university2/college3
        Department department5 = college3.addDepartment();
        department5.setDepartmentName("Dept1 College of Management Boston University");
        System.out.println(department5);
        
        //add department6 in university2/college3
        Department department6 = college3.addDepartment();
        department6.setDepartmentName("Dept2 College of Management Boston University");
        System.out.println(department6);
        
        //add department7 in university2/college4
        Department department7 = college4.addDepartment();
        department7.setDepartmentName("Dept1 College of Law Boston University");
        System.out.println(department7);
        
        //add department8 in university2/college4
        Department department8 = college4.addDepartment();
        department8.setDepartmentName("Dept2 College of Law Boston University");
        System.out.println(department8);
        
        
        //add faculty members
        Facultydirectory facultydirectory1 = department1.getFacultydirectory();
        Faculty faculty1 = facultydirectory1.addFaculty();
        faculty1.setFacultyID("F001");
        //call person class make an object and add it to the faculty
        Person person1 = new Person("Dave", "Jones", "SSN001");
        faculty1.setPerson(person1);
        
        Facultydirectory facultydirectory2 = department1.getFacultydirectory();
        Faculty faculty2 = facultydirectory2.addFaculty();
        faculty2.setFacultyID("F002");
        //call person class make an object and add it to the faculty
        Person person2 = new Person("Derek", "James", "SSN002");
        faculty2.setPerson(person2);
        
        Facultydirectory facultydirectory3 = department2.getFacultydirectory();
        Faculty faculty3 = facultydirectory3.addFaculty();
        faculty3.setFacultyID("F003");
        //call person class make an object and add it to the faculty
        Person person3 = new Person("Taylor", "Swift", "SSN003");
        faculty3.setPerson(person3);
        
        Facultydirectory facultydirectory4 = department2.getFacultydirectory();
        Faculty faculty4 = facultydirectory4.addFaculty();
        faculty4.setFacultyID("F004");
        //call person class make an object and add it to the faculty
        Person person4 = new Person("Lady", "Gaga", "SSN004");
        faculty4.setPerson(person4);
        
        Facultydirectory facultydirectory5 = department3.getFacultydirectory();
        Faculty faculty5 = facultydirectory5.addFaculty();
        faculty5.setFacultyID("F005");
        //call person class make an object and add it to the faculty
        Person person5 = new Person("Mike", "Davis", "SSN005");
        faculty5.setPerson(person5);
        
        Facultydirectory facultydirectory6 = department3.getFacultydirectory();
        Faculty faculty6 = facultydirectory6.addFaculty();
        faculty3.setFacultyID("F006");
        //call person class make an object and add it to the faculty
        Person person6 = new Person("Elton", "John", "SSN006");
        faculty6.setPerson(person6);
        
        Facultydirectory facultydirectory7 = department4.getFacultydirectory();
        Faculty faculty7 = facultydirectory7.addFaculty();
        faculty7.setFacultyID("F007");
        //call person class make an object and add it to the faculty
        Person person7 = new Person("Kim", "Kardashian", "SSN007");
        faculty7.setPerson(person7);
        
        Facultydirectory facultydirectory8 = department4.getFacultydirectory();
        Faculty faculty8 = facultydirectory8.addFaculty();
        faculty8.setFacultyID("F008");
        //call person class make an object and add it to the faculty
        Person person8 = new Person("Vin", "Diesel", "SSN008");
        faculty8.setPerson(person8);
        
        Facultydirectory facultydirectory9 = department5.getFacultydirectory();
        Faculty faculty9 = facultydirectory9.addFaculty();
        faculty9.setFacultyID("F009");
        //call person class make an object and add it to the faculty
        Person person9 = new Person("Ian", "Sommerhalder", "SSN009");
        faculty9.setPerson(person9);
        
        Facultydirectory facultydirectory10 = department5.getFacultydirectory();
        Faculty faculty10 = facultydirectory10.addFaculty();
        faculty10.setFacultyID("F0010");
        //call person class make an object and add it to the faculty
        Person person10 = new Person("Nina", "Garcia", "SSN0010");
        faculty10.setPerson(person10);
        
        Facultydirectory facultydirectory11 = department6.getFacultydirectory();
        Faculty faculty11 = facultydirectory11.addFaculty();
        faculty11.setFacultyID("F0011");
        //call person class make an object and add it to the faculty
        Person person11 = new Person("Naomi", "Smart", "SSN0011");
        faculty11.setPerson(person11);
        
        Facultydirectory facultydirectory12 = department6.getFacultydirectory();
        Faculty faculty12 = facultydirectory12.addFaculty();
        faculty3.setFacultyID("F0012");
        //call person class make an object and add it to the faculty
        Person person12 = new Person("Donald", "Trump", "SSN0012");
        faculty12.setPerson(person12);
        
        Facultydirectory facultydirectory13 = department7.getFacultydirectory();
        Faculty faculty13 = facultydirectory13.addFaculty();
        faculty3.setFacultyID("F0013");
        //call person class make an object and add it to the faculty
        Person person13 = new Person("Hillary", "Clinton", "SSN0013");
        faculty13.setPerson(person13);
        
        Facultydirectory facultydirectory14 = department7.getFacultydirectory();
        Faculty faculty14 = facultydirectory14.addFaculty();
        faculty14.setFacultyID("F0014");
        //call person class make an object and add it to the faculty
        Person person14 = new Person("Alex", "Ferguson", "SSN0014");
        faculty14.setPerson(person14);
        
        Facultydirectory facultydirectory15 = department8.getFacultydirectory();
        Faculty faculty15 = facultydirectory15.addFaculty();
        faculty15.setFacultyID("F0015");
        //call person class make an object and add it to the faculty
        Person person15 = new Person("Jack", "Sinclar", "SSN0015");
        faculty15.setPerson(person15);
        
        Facultydirectory facultydirectory16 = department8.getFacultydirectory();
        Faculty faculty16 = facultydirectory16.addFaculty();
        faculty16.setFacultyID("F0016");
        //call person class make an object and add it to the faculty
        Person person16 = new Person("Lilly", "Singh", "SSN0016");
        faculty16.setPerson(person16);
        
        Coursecatalog coursecatalog1 = department1.getCoursecatalog() ;
        // Add courses
        Course course1 = coursecatalog1.addCourse() ;
        course1.setCourseName("MGMT Course-1");
        course1.setCourseCRN("01");
        course1.setCourseCredits(4);
        course1.setCourseDepartment("MGMT");
        course1.setCourseDescription("This course gives knowledge about management and maybe a little bit of Java!");
        
        Course course2 = coursecatalog1.addCourse() ;
        course2.setCourseName("MGMT Course-2");
        course2.setCourseCRN("11");
        course2.setCourseCredits(4);
        course2.setCourseDepartment("MGMT");
        course2.setCourseDescription("This is the second part of the Management course");
        
        Coursecatalog coursecatalog2 = department2.getCoursecatalog();
        Course course3 = coursecatalog2.addCourse() ;
        course3.setCourseName("LAW Best Practices-1");
        course3.setCourseCRN("12");
        course3.setCourseCredits(4);
        course3.setCourseDepartment("LAW");
        course3.setCourseDescription("Take this course to know more about law best practices");
        course3.setOptional();

        Course course4 = coursecatalog2.addCourse() ;
        course4.setCourseName("LAW Advanced Practices-2");
        course4.setCourseCRN("13");
        course4.setCourseCredits(4);
        course4.setCourseDepartment("LAW");
        course4.setCourseDescription("This is the advanced level of the course-1 Law best practices");  
        course4.setMandatory();
        
        Degree degree1 = department1.addDegree() ;
        degree1.setDegreeName("MASTERS IN LAW");
        
        Degree degree2 = department2.addDegree() ;
        degree2.setDegreeName("MASTERS IN LAW");
     
        
   
        
        
          // StudentDirectory
        Studentdirectory studentDirectory = department1.getStudentdirectory() ;
            //Add students
        Student student1 = studentDirectory.addStudent() ;
        person1 = new Person("Karan", "Dutt", "KDSSN009") ;
        student1.setPerson(person1);
        student1.setStudentID("001229215");
        degree1.addStudent(student1);
        Transcript transcript=student1.getTranscript();
        Courseload courseload1=student1.getCourseload();
        courseload1.setAverageGrade(4);
        courseload1.setSemesterGPA(3.5);
        courseload1.setSemesterGPA(3.8);
        Seatassignment seatassignment1=courseload1.addSeatassignment();
        seatassignment1.setGrade(3.5);
        Courseschedule courseschedule1=department1.addCourseschedule();
        courseschedule1.setSemester("Sem1");
        Courseoffering courseoffering1=courseschedule1.addCourseoffering();
        courseoffering1.setHall("Dodge Hall");
        courseoffering1.setRoom("DH-69");
        courseoffering1.setSeatsOccupied(8);
        courseoffering1.setCourse(course1);
        courseoffering1.setSeatsRemaining(2);
       return student1;
      

     
            
            //Add students
    
    }
}   
        
        
        
        
        
        
        
        
        
        
        
        
          
        
         
        
         
         
         
         
         
         
         
    

