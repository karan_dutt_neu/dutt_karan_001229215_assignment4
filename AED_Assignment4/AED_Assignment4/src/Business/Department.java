/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Department {
    //declaration of private instance variables
    private String departmentName;
    private Studentdirectory studentdirectory;
    private Facultydirectory facultydirectory;
    private Coursecatalog coursecatalog; 
    private ArrayList<Degree> degreelist;
    private ArrayList<Courseschedule> courseschedulelist;
    
    //constructor
    public Department(){
        studentdirectory = new Studentdirectory();
        facultydirectory = new Facultydirectory();
        coursecatalog = new Coursecatalog();
        degreelist = new ArrayList<Degree>();
        courseschedulelist = new ArrayList<Courseschedule>();
    }
    
    //getters and setters
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Studentdirectory getStudentdirectory() {
        return studentdirectory;
    }

    public void setStudentdirectory(Studentdirectory studentdirectory) {
        this.studentdirectory = studentdirectory;
    }

    public Facultydirectory getFacultydirectory() {
        return facultydirectory;
    }

    public void setFacultydirectory(Facultydirectory facultydirectory) {
        this.facultydirectory = facultydirectory;
    }

    public Coursecatalog getCoursecatalog() {
        return coursecatalog;
    }

    public void setCoursecatalog(Coursecatalog coursecatalog) {
        this.coursecatalog = coursecatalog;
    }

    public ArrayList<Degree> getDegreelist() {
        return degreelist;
    }

    public void setDegreelist(ArrayList<Degree> degreelist) {
        this.degreelist = degreelist;
    }

    public ArrayList<Courseschedule> getCourseschedulelist() {
        return courseschedulelist;
    }

    public void setCourseschedulelist(ArrayList<Courseschedule> courseschedulelist) {
        this.courseschedulelist = courseschedulelist;
    }
    
    //methods
    public Courseschedule addCourseschedule(){
        Courseschedule courseschedule = new Courseschedule();
        courseschedulelist.add(courseschedule);
        return courseschedule;
    }
    
    public void deleteCourseschedule(Courseschedule courseschedule){
        courseschedulelist.remove(courseschedule);
    }
    
    public Degree addDegree(){
        Degree degree = new Degree();
        degreelist.add(degree);
        return degree;
    }
    
    public void deleteDegree(Degree degree){
        degreelist.remove(degree);
    }
    
    //this method gets a course schedule of a particular semester
    //tutorialspoint.com/java/java_string_matches.htm matches method
    public Courseschedule getCourseschedule(String semester){
        for(Courseschedule courseschedule : courseschedulelist){
            if(courseschedule.getSemester().matches(semester)){
                return courseschedule;
            }
        }
        return null;
    }
    
    @Override
    public String toString(){
        return departmentName;
    }
}
