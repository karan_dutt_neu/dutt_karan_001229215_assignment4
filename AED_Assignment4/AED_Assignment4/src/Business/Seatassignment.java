/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Seatassignment {
    private Courseoffering courseoffered;
    private double grade;


    
    public Courseoffering getCourseoffered() {
        return courseoffered;
    }

    public void setCourseoffered(Courseoffering courseoffered) {
        this.courseoffered = courseoffered;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
    
    
}
