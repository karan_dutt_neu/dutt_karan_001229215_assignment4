/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Region {
    //declaration of private instance variables
    private ArrayList<University> universitydirectory;
    
    //constructor
    public Region(){
        universitydirectory = new ArrayList<University>();
    }
    
    //getter and setter
    public ArrayList<University> getUniversitydirectory() {
        return universitydirectory;
    }

    public void setUniversitydirectory(ArrayList<University> universitydirectory) {
        this.universitydirectory = universitydirectory;
    }
    
    //methods
    public University addUniversity(){
        University university = new University();
        universitydirectory.add(university);
        return university;
    }
    
    public void deleteUniversity(University university){
        universitydirectory.remove(university);
    }
    
    
}
