/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class Studentdirectory {
    
    //declaration of private instance variables
    private ArrayList<Student> studentdirectory;
    private int studentCount = 0;
    
    //constructor
    public Studentdirectory(){
        studentdirectory = new ArrayList<Student>();
    }
    
    //getters and setters
    public ArrayList<Student> getStudentdirectory() {
        return studentdirectory;
    }

    public void setStudentdirectory(ArrayList<Student> studentdirectory) {
        this.studentdirectory = studentdirectory;
    }

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }
    
    //methods
    //in this method add a student and also increase the studentCount by 1 everytime we add a new student
    public Student addStudent(){
        Student student = new Student();
        studentdirectory.add(student);
        studentCount++;
        return student;
    }
    
    public void deleteStudent(Student student){
        studentdirectory.remove(student);
        studentCount--;
    }
    
}
