/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author karandutt
 */
public class University {
    //declaration of private instance 
    private String universityName;
    private ArrayList<College> collegedirectory;
    
    //constructor
    public University(){
        collegedirectory = new ArrayList<College>();
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public ArrayList<College> getCollegedirectory() {
        return collegedirectory;
    }

    public void setCollegedirectory(ArrayList<College> collegedirectory) {
        this.collegedirectory = collegedirectory;
    }
    
    //methods
    public College addCollege(){
        College college = new College();
        collegedirectory.add(college);
        return college;
    }
    
    public void deleteCollege(College college){
        collegedirectory.remove(college);
    }
    
    @Override
    public String toString(){
        return universityName;
    }
}
